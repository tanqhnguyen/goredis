module gitlab.com/tanqhnguyen/goredis

go 1.14

require (
	github.com/go-redis/redis/v8 v8.0.0-beta.5
	github.com/onsi/ginkgo v1.13.0 // indirect
	github.com/tanqhnguyen/envconfig v1.5.6
)
