package goredis

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
	"github.com/tanqhnguyen/envconfig"
)

// RedisConfig is the configuration of a redis server
type RedisConfig struct {
	Database int    `envconfig:"database" default:"0"`
	Password string `envconfig:"password" default:"" file_content:"true"`
	Host     string `envconfig:"host" default:"127.0.0.1"`
	Port     int    `envconfig:"port" default:"6379"`
}

// RedisClient is a thin wrapper on top of *redisClient to provide extra commands
type RedisClient struct {
	*redis.Client
	Config *RedisConfig
}

// ZPopByScore pops the members within min/max
func (c *RedisClient) ZPopByScore(ctx context.Context, key string, opt *redis.ZRangeBy) *redis.StringSliceCmd {
	ZPopByScore := redis.NewScript(`
		local members = redis.call("ZRANGEBYSCORE", KEYS[1], unpack(ARGV))

		if next(members) == nil then
			return members
		end

		redis.call("ZREM", KEYS[1], unpack(members))
		return members
	`)

	args := []interface{}{opt.Min, opt.Max}

	if opt.Offset != 0 || opt.Count != 0 {
		args = append(
			args,
			"LIMIT",
			opt.Offset,
			opt.Count,
		)
	}

	result, err := ZPopByScore.Run(context.Background(), c, []string{key}, args...).Result()
	if results, ok := result.([]interface{}); ok {
		str := make([]string, 0)
		for _, r := range results {
			str = append(str, r.(string))
		}

		return redis.NewStringSliceResult(str, nil)
	}

	return redis.NewStringSliceResult(nil, err)
}

// NewClientFromEnvironment creates a new redis client based on the env config
func NewClientFromEnvironment() *RedisClient {
	var fromEnv RedisConfig
	err := envconfig.Process("REDIS", &fromEnv)

	if err != nil {
		panic(err)
	}

	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", fromEnv.Host, fromEnv.Port),
		Password: fromEnv.Password,
		DB:       fromEnv.Database,
	})

	_, err = client.Ping(context.Background()).Result()
	if err != nil {
		panic(err)
	}

	return &RedisClient{client, &fromEnv}
}
